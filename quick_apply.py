import numpy as np
import torch
import uproot4 as up
import uproot
from models.ResNet import resnet18
from utils.parallel import DataParallelModel
import time
import sys

ch = sys.argv[1]

input_file = "./data/{}_CaloHits.root:apply".format(ch)
# input_file = "./data/Sig_Tiny.root:CALOHits"

img_size = 200
heat_size = 112
delta_x = 2 * np.pi / heat_size
delta_y = 8e3 / heat_size
step_size = 256

# output file
output_file = "{}_out.root".format(ch)
out_file = uproot.recreate(output_file)
out_file['llp'] = uproot.newtree({
    "n1_label": np.float,
    "n1_heat_label": np.float,
    "n2_label": np.float,
    "n2_heat_label": np.float,
    "truth_R": np.float,
    "truth_phi": np.float,
    "n1_predict_mid_R": np.float,
    "n1_predict_mid_phi": np.float
})

model1 = resnet18(nb_ch=2)
model1 = DataParallelModel(model1)
model2 = resnet18(nb_ch=2)
model2 = DataParallelModel(model2)
model_fine = resnet18(nb_ch=2)
model_fine = DataParallelModel(model_fine)

model1.load_state_dict(torch.load('./saved_model/resnet18_ZH.pt'))
model2.load_state_dict(torch.load('./saved_model/resnet18_eeqq.pt'))
# model1.load_state_dict(torch.load('./saved_model/resnet18_ZH_0.9_0.1.pt'))
# model2.load_state_dict(torch.load('./saved_model/resnet18_eeqq_0.9_0.1.pt'))
model_fine.load_state_dict(torch.load('./saved_model/resnet18_no_bkg_ZH.pt'))

model1.eval()
model2.eval()
model_fine.eval()

input_root = up.open(input_file)
total_n = input_root.num_entries
out_diff = np.zeros((total_n, 2))

i = 0

t_start = time.time()
for batch, report in input_root.iterate(step_size=step_size, library="np", report=True):
    if i % round(total_n / 50) == 1:
        print("==> i = ", i, ": {:6.2f}".format(i / total_n), flush=True)

    # Phase 1
    b_len = report.stop - report.start
    out = np.zeros((b_len, 2, img_size, img_size), dtype=np.float32)
    out[:, 0, ...] = (batch['E'] / 25.).reshape(b_len, img_size, img_size)
    out[:, 1, ...] = (batch['T'] / 200.).reshape(b_len, img_size, img_size)

    out = torch.tensor(out, device='cuda').float()

    # ZH model
    n1_out_label, n1_out_heat = model1(out)
    n1_out_x = np.argmax(n1_out_heat.detach().cpu().numpy().reshape(b_len, heat_size ** 2), axis=1) % heat_size
    n1_out_y = (np.argmax(n1_out_heat.detach().cpu().numpy().reshape(b_len, heat_size ** 2),
                          axis=1) / heat_size).astype(int)

    # eeqq model
    n2_out_label, n2_out_heat = model2(out)

    # truth
    t_R = np.sqrt(batch['SS1_Pos'][:, 0] ** 2 + batch['SS1_Pos'][:, 1] ** 2 + batch['SS1_Pos'][:, 2] ** 2)
    t_Phi = np.arctan2(batch['SS1_Pos'][:, 1], batch['SS1_Pos'][:, 0])
    t_idx_x = ((t_Phi + np.pi) / (2 * np.pi / heat_size)).astype(int)
    t_idx_y = (t_R / (8e3 / heat_size)).astype(int)

    out_file['llp'].extend({
        "n1_label": n1_out_label.detach().cpu().numpy(),
        "n1_heat_label": np.max(n1_out_heat.detach().cpu().numpy().reshape(b_len, heat_size ** 2), axis=1),
        "n2_label": n2_out_label.detach().cpu().numpy(),
        "n2_heat_label": np.max(n2_out_heat.detach().cpu().numpy().reshape(b_len, heat_size ** 2), axis=1),
        "truth_R": t_idx_y,
        "truth_phi": t_idx_x,
        "n1_predict_mid_R": n1_out_y,
        "n1_predict_mid_phi": n1_out_x
    })

    i += 1

time_elapsed = time.time() - t_start
print('Apply complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
print("Done...", flush=True)
