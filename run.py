import numpy as np
import torch
import matplotlib.pyplot as plt
import plotly.express as px

from utils.data_loader import get_data
from utils.train_func import train_model
from utils.data_loader import MyDataset
from utils.parallel import DataParallelModel, DataParallelCriterion

from models.ResNet import resnet50
from models.ResNet import resnet18


def train(sig, bg, channel, model):
    print('Start Training with channel:', channel)

    r, c = 100, 100
    num_epochs = 25
    batch_size = 512
    test_size = 0.2
    lr = 0.005
    use_gpu = True

    torch.backends.cudnn.benchmark = True
    device = torch.device('cuda') if use_gpu else torch.device('cpu')
    dataloaders = get_data(sig=sig,
                           bg=bg,
                           r=r,
                           c=c,
                           batch_size=batch_size,
                           test_size=test_size,
                           use_gpu=use_gpu)
    print("==> Data loading completed.")

    model = model.to(device)
    # criterion = torch.nn.BCELoss()
    parallel_loss = DataParallelCriterion(torch.nn.BCELoss())
    optimizer = torch.optim.Adam(model.parameters(),
                                 lr=lr,
                                 betas=(0.9, 0.999),
                                 eps=1e-08,
                                 weight_decay=1e-4)
    print("==> training start!!")
    model, history = train_model(model=model,
                                 dataloaders=dataloaders,
                                 criterion=parallel_loss,
                                 optimizer=optimizer,
                                 num_epochs=num_epochs,
                                 device=device)
    torch.save(model.state_dict(), 'saved_model/resnet50_' + channel + '.pt')

    plot_params = {
        'linewidth': 1.5,
        'alpha': 1,
    }

    fig, ax = plt.subplots(1, 1, figsize=(8, 5), constrained_layout=True)
    for key in history:
        ax.plot([x + 1 for x in range(num_epochs)], history[key], label=key, **plot_params)
    ax.legend(loc='best', fontsize=15, edgecolor='k')
    ax.set_xlabel('epoch', fontsize=15)
    ax.grid(True)
    ax.tick_params(labelsize=12)
    # plt.show()
    fig.savefig('./plots/Train_Cur_' + channel + '.png', dpi=200)


def apply(model, data, r=100, c=100, batch_size=512, use_gpu=True):
    dataset = MyDataset(data, np.zeros((len(data), 1)), r, c)
    pin_memory = True if use_gpu else False
    device = torch.device('cuda') if use_gpu else torch.device('cpu')
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        num_workers=4,
        pin_memory=pin_memory, )
    model = model.to(device)
    model.eval()
    for i, (input, _) in enumerate(dataloader):
        input = input.to(device).float()
        output = model(input)

        for j in range(len(output)):
            batch_out = output[j].cpu().detach().numpy()
            if (i == 0 and j == 0):
                out = batch_out
            else:
                out = np.concatenate((out, batch_out), axis=0)

    return out


if __name__ == '__main__':
    from utils.read_root import read_arr

    # channel = 'eeqq'
    channel = 'ZH'
    Sig = read_arr("./data/Sig_CaloHits_raw.root:ImageHits")
    nb_sig = (int)(len(Sig) / 2)
    if (channel == 'eeqq'):
        Bkg = read_arr("./data/eeqq_CaloHits_raw.root:ImageHits")
        nb_bkg = (int)(len(Bkg) / 5)
    else:
        Bkg = read_arr("./data/ZH_CaloHits_1207.root:ImageHits")
        nb_bkg = (int)(len(Bkg) / 2)

    np.random.shuffle(Sig)
    np.random.shuffle(Bkg)

    sig = Sig[:nb_sig]
    bg = Bkg[:nb_bkg]

    model = resnet18(nb_ch=1)
    model = DataParallelModel(model)

    train(sig, bg, channel, model)

    sig = Sig[nb_sig:]
    bg = Bkg[nb_bkg:]

    batch_size = 1024
    use_gpu = True

    model.load_state_dict(torch.load('./saved_model/resnet50_' + channel + '.pt'))

    sig_out = apply(model, sig)
    bg_out = apply(model, bg)

    for cut_value in [0.99, 0.999, 0.9995, 0.9999]:
        print("Cut value = ", cut_value)
        print("Sig: ", len(sig_out[sig_out > cut_value]), "/", len(sig_out), " = ",
              len(sig_out[sig_out > cut_value]) / len(sig_out))
        print("Bkg: ", len(bg_out[bg_out > cut_value]), "/", len(bg_out), " = ",
              len(bg_out[bg_out > cut_value]) / len(bg_out))

    plot_params = {
        'bins': 25,
        'range': (0, 1.0),
        'linewidth': 1.5,
        'histtype': 'step',
        'alpha': 1.0,
        'density': 0
    }

    fig, ax = plt.subplots(1, 1, figsize=(8, 6), constrained_layout=True)
    ax.hist(sig_out, label=r'$llp$', **plot_params)
    ax.hist(bg_out, label=r'$' + channel + '$', **plot_params)

    ax.legend(loc='upper center', fontsize=15, edgecolor='k')
    ax.set_xlabel('ML score', fontsize=15)
    ax.grid(True)
    ax.tick_params(labelsize=12)
    ax.set_yscale('log')
    plt.show()
    fig.savefig('./plots/Test_MLScore_' + channel + '.png', dpi=200)

    from sklearn.metrics import roc_auc_score, roc_curve

    y_true = np.concatenate((np.ones(len(sig_out)), np.zeros(len(bg_out))), axis=0)
    y_pred = np.concatenate((sig_out, bg_out), axis=0)
    auc = roc_auc_score(y_true, y_pred)
    ax.set_ylabel('background acceptance', fontsize=15)
    ax.grid(True)
    ax.tick_params(labelsize=12)
    plt.show()
    fpr, tpr, _ = roc_curve(y_true, y_pred)

    plot_params = {
        'linewidth': 1.5,
        'alpha': 1,
    }

    fig, ax = plt.subplots(1, 1, figsize=(5, 5), constrained_layout=True)
    ax.plot(tpr, 1 - fpr, label='AUC=%.3f' % auc, **plot_params)
    # ax.set_yscale('log')
    # ax.set_xscale('log')
    ax.legend(loc='best', fontsize=15, edgecolor='k')
    ax.set_xlabel('signal acceptance', fontsize=15)
    fig.savefig('./plots/AUC_' + channel + '.png', dpi=200)

