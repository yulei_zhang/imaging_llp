import numpy as np
import torch
import uproot4 as up
import uproot
from models.ResNet import resnet18
from utils.parallel import DataParallelModel
import sys

ch = sys.argv[1]

input_file = "./data/{}_CaloHits.root:apply".format(ch)
img_size = 200
heat_size = 112
delta_x = 2 * np.pi / heat_size
delta_y = 8e3 / heat_size

# output file
output_file = "{}_out.root".format(ch)
out_file = uproot.recreate(output_file)
out_file['llp'] = uproot.newtree({
    #"truth_R": np.float,
    #"truth_phi": np.float,
    "n1_label": np.float,
    "n1_heat_label": np.float,
    #"n1_predict_R": np.float,
    #"n1_predict_phi": np.float,
    #"n1_predict_mid_R": np.float,
    #"n1_predict_mid_phi": np.float,
    "n2_label": np.float,
    "n2_heat_label": np.float
    #"n2_predict_mid_R": np.float,
    #"n2_predict_mid_phi": np.float
})


model1 = resnet18(nb_ch=2)
model1 = DataParallelModel(model1)
model2 = resnet18(nb_ch=2)
model2 = DataParallelModel(model2)
model_fine = resnet18(nb_ch=2)
model_fine = DataParallelModel(model_fine)

model1.load_state_dict(torch.load('./saved_model/resnet18_ZH.pt'))
model2.load_state_dict(torch.load('./saved_model/resnet18_eeqq.pt'))
#model1.load_state_dict(torch.load('./saved_model/resnet18_ZH_0.9_0.1.pt'))
#model2.load_state_dict(torch.load('./saved_model/resnet18_eeqq_0.9_0.1.pt'))
model_fine.load_state_dict(torch.load('./saved_model/resnet18_no_bkg_ZH.pt'))

model1.eval()
model2.eval()
model_fine.eval()

input_root = up.open(input_file)
total_n = input_root.num_entries
out_diff = np.zeros((total_n, 2))

i = 0
for batch, report in input_root.iterate(step_size=1, library="np", report=True):
    if i % round(total_n/50) == 1:
        print("==> i = ", i, ": {:6.2f}".format(i/total_n), flush=True)

    # Phase 1
    b_len = report.stop - report.start
    out = np.zeros((1, 2, img_size, img_size), dtype=np.float32)
    out[0,0] = (batch['E'] / 25.).reshape(b_len, 1, img_size, img_size)
    out[0,1] = (batch['T'] / 200.).reshape(b_len, 1, img_size, img_size)

    out = torch.tensor(out, device='cuda').float()

    # ZH model
    n1_out_label, n1_out_heat = model1(out)
    n1_out_x = round(np.argmax(n1_out_heat.detach().cpu().numpy()[0][0]) % heat_size) * delta_x
    n1_out_y = round(np.argmax(n1_out_heat.detach().cpu().numpy()[0][0]) / heat_size) * delta_y - np.pi

    # eeqq model
    n2_out_label, n2_out_heat = model2(out)
    n2_out_x = round(np.argmax(n2_out_heat.detach().cpu().numpy()[0][0]) % heat_size) * delta_x 
    n2_out_y = round(np.argmax(n2_out_heat.detach().cpu().numpy()[0][0]) / heat_size) * delta_y - np.pi

    # print(out_x, out_y)
#   # Phase 2
#
#   # First to pixelate the target region
#   phi_min = (n1_out_x + np.pi) - 6 * delta_x
#   phi_max = phi_min + 2 * 6 * delta_x
#   R_min = n1_out_y - 6 * delta_y
#   R_max = R_min + 2 * 6 * delta_y
#
#   fined_out = np.zeros((1, 2, img_size, img_size))
#   max_E = np.zeros((img_size, img_size))
#   for hits_i in range(batch['COL_Size'][0]):
#       R = np.sqrt(batch['COL_X'][0][hits_i] ** 2 + batch['COL_Y'][0][hits_i] ** 2 + batch['COL_Z'][0][hits_i] ** 2)
#       Phi = np.arctan2(batch['COL_Y'][0][hits_i], batch['COL_X'][0][hits_i])
#       idx_x = round((Phi + np.pi - phi_min) / ((phi_max - phi_min) / img_size))
#       idx_y = round((R - R_min) / ((R_max - R_min) / img_size))
#       if idx_y < 0 or idx_x < 0 or idx_x >= img_size or idx_y >= img_size:
#           continue
#
#       if abs(batch['COL_T'][0][hits_i]) < 200.:
#           if batch['COL_E'][0][hits_i] >= max_E[idx_x, idx_y]:
#               fined_out[0, 1, idx_x, idx_y] = batch['COL_T'][0][hits_i]
#           fined_out[0, 0, idx_x, idx_y] += batch['COL_E'][0][hits_i]
#           max_E[idx_x, idx_y] = batch['COL_E'][0][hits_i] if batch['COL_E'][0][hits_i] >= max_E[idx_x, idx_y] \
#               else max_E[idx_x, idx_y]
#
#   for hits_i in range(batch['TRK_Size'][0]):
#       R = np.sqrt(batch['TRK_X'][0][hits_i] ** 2 + batch['TRK_Y'][0][hits_i] ** 2 + batch['TRK_Z'][0][hits_i] ** 2)
#       Phi = np.arctan2(batch['TRK_Y'][0][hits_i], batch['TRK_X'][0][hits_i])
#       idx_x = round((Phi + np.pi - phi_min) / ((phi_max - phi_min) / img_size))
#       idx_y = round((R - R_min) / ((R_max - R_min) / img_size))
#       if idx_y < 0 or idx_x < 0 or idx_x >= img_size or idx_y >= img_size:
#           continue
#
#       if abs(batch['TRK_T'][0][hits_i]) < 200.:
#           if batch['TRK_E'][0][hits_i] >= max_E[idx_x, idx_y]:
#               fined_out[0, 1, idx_x, idx_y] = batch['TRK_T'][0][hits_i]
#           fined_out[0, 0, idx_x, idx_y] = 1.
#           max_E[idx_x, idx_y] = batch['TRK_E'][0][hits_i] if batch['TRK_E'][0][hits_i] >= max_E[idx_x, idx_y] \
#               else max_E[idx_x, idx_y]
#
#   fined_out = torch.tensor(fined_out, device='cuda').float()
#   fined_out_label, fined_out_heat = model_fine(out)
#   idx_x = round(np.argmax(fined_out_heat.detach().cpu().numpy()[0][0]) / heat_size)
#   idx_y = round(np.argmax(fined_out_heat.detach().cpu().numpy()[0][0]) % heat_size)
#   final_x = round(np.argmax(fined_out_heat.detach().cpu().numpy()[0][0]) / heat_size) * (
#           (phi_max - phi_min) / heat_size) + phi_min - np.pi
#   final_y = round(np.argmax(fined_out_heat.detach().cpu().numpy()[0][0]) % heat_size) * (
#           (R_max - R_min) / heat_size) + R_min
#
#   # truth
#   t_R = np.sqrt(batch['SS1_Pos'][0][0] ** 2 + batch['SS1_Pos'][0][1] ** 2 + batch['SS1_Pos'][0][2] ** 2)
#   t_Phi = np.arctan2(batch['SS1_Pos'][0][1], batch['SS1_Pos'][0][0])
#   t_idx_x = round((t_Phi + np.pi - phi_min) / ((phi_max - phi_min) / heat_size))
#   t_idx_y = round((t_R - R_min) / ((R_max - R_min) / heat_size))

    # print("Truth: ({}, {}); Predict: ({}, {})".format(t_idx_x, t_idx_y, idx_x, idx_y))
    # print("Truth: ({:6.0f}, {:6.3f}); Predict: ({:6.0f}, {:6.3f})".format(t_R, t_Phi, final_y, final_x))

    out_file['llp'].extend({
        #"truth_R": np.array([t_R]),
        #"truth_phi": np.array([t_Phi]),
        "n1_label": np.array([n1_out_label]),
        "n1_heat_label": np.array([np.max(n1_out_heat.detach().cpu().numpy()[0][0])]),
        #"n1_predict_R": np.array([final_y]),
        #"n1_predict_phi": np.array([final_x]),
        #"n1_predict_mid_R": np.array([n2_out_y]),
        #"n1_predict_mid_phi": np.array([n2_out_x]),
        "n2_label": np.array([n2_out_label]),
        "n2_heat_label": np.array([np.max(n2_out_heat.detach().cpu().numpy()[0][0])]),
        #"n2_predict_mid_R": np.array([n2_out_y]),
        #"n2_predict_mid_phi": np.array([n2_out_x])
    })

    #out_diff[i, 0] = final_y - t_R
    #out_diff[i, 1] = final_x - t_Phi

    i += 1
