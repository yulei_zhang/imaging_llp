import numpy as np
import uproot_methods
import awkward

def to_p4(x):
    """
    input: (x, y, z, E) in shape of (nb_evt, max_nb_particles, 4), zero padded
    output: TLorentzVectorArray in shape of (nb_evt, nb_particles,)
    """
    mask = x[:, :, 0] > 0
    n_particles = np.sum(mask, axis=1)
    X = awkward.JaggedArray.fromcounts(n_particles, x[:, :, 0][mask])
    Y = awkward.JaggedArray.fromcounts(n_particles, x[:, :, 1][mask])
    Z = awkward.JaggedArray.fromcounts(n_particles, x[:, :, 2][mask])
    #mass = awkward.JaggedArray.fromcounts(n_particles, np.zeros(x[:, :, 2].shape)[mask])
    E = awkward.JaggedArray.fromcounts(n_particles, x[:, :, 3][mask])
    #p4 = uproot_methods.TLorentzVectorArray.from_ptetaphim(pt, eta, phi, mass)
    p4 = uproot_methods.TLorentzVectorArray.from_cartesian(X, Y, Z, E)
    return p4

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)