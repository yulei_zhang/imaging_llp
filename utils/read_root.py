import numpy as np
import uproot4 as up
import os


def read_arr(input_file):
    llp_ECAL = up.open(input_file)

    nb_entries = llp_ECAL.num_entries

    # E = llp_ECAL.array("E")
    E = llp_ECAL["E"].array(library="np")

    del llp_ECAL

    max_par = 100
    nb_ch = 1
    OUT = np.zeros((nb_entries, nb_ch, max_par, max_par), dtype=np.float32)
    for j in range(nb_entries):

        if (j % (int)(nb_entries / 20) == 0):
            print("evt: ", j, " / ", nb_entries, ", [", j / nb_entries * 100, "%]")

        OUT[j, 0] = E[j].reshape(max_par, max_par)

    del E

    return OUT
